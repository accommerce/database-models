const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema({
    order: {
        type: mongoose.Types.ObjectId,
        ref: 'Orders',
        required: true,
    },
    shop: {
        type: mongoose.Types.ObjectId,
        ref: 'Shops',
    },
    seller: {
        type: mongoose.Types.ObjectId,
        ref: 'Users',
    },
    customer: {
        type: mongoose.Types.ObjectId,
        ref: 'Users',
    },
    amount: {
        type: Number,
        min: 0,
        required: true,
    },
    capturedTime: {
        type: Date,
        required: true,
    },
})

schema.index({ seller: 1, customer: 1, shop: 1 })
schema.index({ capturedTime: 1 })

module.exports = schema
