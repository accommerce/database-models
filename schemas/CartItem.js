const mongoose = require('mongoose')
const Schema = mongoose.Schema

const cartItemsSchema = new Schema(
    {
        customer: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: 'Users',
        },
        shop: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: 'Shops',
        },
        product: {
            type: mongoose.Types.ObjectId,
            ref: 'Products',
            required: true,
        },
        size: {
            type: String,
            required: true,
        },
        quantity: {
            type: Number,
            required: true,
            min: 1,
        },
    },
    { timestamps: true }
)

cartItemsSchema.index({ seller: 1, customer: 1, product: 1 })

module.exports = cartItemsSchema
