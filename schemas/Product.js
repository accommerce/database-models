const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        shop: {
            type: mongoose.Types.ObjectId,
            ref: 'Shops',
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
        rating: {
            type: Number,
            default: 5,
            max: 5,
            min: 1,
        },
        category: {
            type: String,
        },
        images: {
            type: [String],
            default: [],
        },
        price: {
            type: Number,
            min: 0,
            required: true,
        },
        originalPrice: {
            type: Number,
            min: 0,
            required: true,
        },
        views: {
            type: Number,
            default: 0,
        },
        sold: {
            type: Number,
            default: 0,
        },
        deletedAt: {
            type: Date,
        },
    },
    { timestamps: true }
)

productSchema.index({ name: 'text', description: 'text', category: 'text' })
productSchema.index({ seller: 1 })

module.exports = productSchema
