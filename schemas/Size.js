const mongoose = require('mongoose')
const Schema = mongoose.Schema

const sizeSchema = new Schema(
    {
        product: {
            type: mongoose.Types.ObjectId,
            ref: 'Products',
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        numberInStock: {
            type: Number,
            required: true,
            min: 0,
        },
    },
    { timestamps: true }
)

module.exports = sizeSchema
