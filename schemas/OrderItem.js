const mongoose = require('mongoose')
const Schema = mongoose.Schema

const orderItemsSchema = new Schema(
    {
        order: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: 'Orders',
        },
        product: {
            type: mongoose.Types.ObjectId,
            ref: 'Products',
        },
        size: {
            type: String,
            required: true,
        },
        quantity: {
            type: Number,
            required: true,
            min: 1,
        },
        price: {
            type: Number,
            min: 0,
            required: true,
        },
    },
    { timestamps: true }
)

orderItemsSchema.index({ order: 1, product: 1 })

module.exports = orderItemsSchema
