const mongoose = require('mongoose')
const Schema = mongoose.Schema

const commentSchema = new Schema(
    {
        author: {
            type: mongoose.Types.ObjectId,
            ref: 'Users',
            required: true,
        },
        product: {
            type: mongoose.Types.ObjectId,
            ref: 'Products',
            required: true,
        },
        comment: {
            type: String,
        },
        rating: {
            type: Number,
            min: 1,
            max: 5,
            required: true,
        },
        images: [String],
    },
    { timestamps: true }
)

commentSchema.index({ createdAt: -1, updatedAt: -1 })

module.exports = commentSchema
