const mongoose = require('mongoose')
const Schema = mongoose.Schema

const discountSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        type: {
            type: String,
            enum: ['percentage', 'fixed'],
            required: true,
        },
        amount: {
            type: Number,
            default: 0,
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        deletedAt: {
            type: Date,
        },
    },
    { timestamps: true }
)

discountSchema.index({ name: 1 })

module.exports = discountSchema
