const mongoose = require('mongoose')
const Schema = mongoose.Schema

const orderSchema = new Schema(
    {
        customer: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: 'Users',
        },
        shop: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: 'Shops',
        },
        sellingAddress: {
            type: String,
            required: true,
        },
        receivingAddress: {
            type: String,
            required: true,
        },
        discount: {
            type: mongoose.Types.ObjectId,
            ref: 'Discounts',
        },
        shippingCost: {
            type: Number,
            default: 0,
        },
        status: {
            type: String,
            enum: [
                'Waiting for seller confirm',
                'In transit',
                'Delivered',
                'Cancelled by customer',
                'Cancelled by seller',
            ],
            default: 'Waiting for seller confirm',
        },
    },
    { timestamps: true }
)

orderSchema.index({ customer: 1, seller: 1 })
orderSchema.index({ status: 1 })

module.exports = orderSchema
