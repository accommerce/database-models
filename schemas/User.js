const mongoose = require('mongoose')
const Schema = mongoose.Schema

const passportLocalMongoose = require('passport-local-mongoose')

const userSchema = new Schema(
    {
        firstName: {
            type: String,
            required: true,
        },
        lastName: {
            type: String,
            required: true,
        },
        phoneNumber: {
            type: String,
            required: true,
            unique: true,
        },
        email: {
            type: String,
        },
        address: {
            type: String,
        },
        roles: {
            type: [
                {
                    type: String,
                    enum: ['admin', 'customer', 'seller'],
                },
            ],
            default: ['customer'],
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        deletedAt: {
            type: Date,
        },
    },
    { timestamps: true }
)

userSchema.plugin(passportLocalMongoose)
userSchema.index({ username: 1 })

module.exports = userSchema
