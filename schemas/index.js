module.exports = {
    CartItem: require('./CartItem'),
    Comment: require('./Comment'),
    Discount: require('./Discount'),
    Order: require('./Order'),
    OrderItem: require('./OrderItem'),
    Product: require('./Product'),
    Size: require('./Size'),
    User: require('./User'),
    Shop: require('./Shop'),
    ProductStatistics: require('./ProductStatistics'),
    OrderStatistics: require('./OrderStatistics'),
}
