const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema({
    product: {
        type: mongoose.Types.ObjectId,
        ref: 'Products',
        required: true,
    },
    price: {
        type: Number,
        min: 0,
        required: true,
    },
    capturedTime: {
        type: Date,
        required: true,
    },
})

schema.index({ product: 1, capturedTime: 1 })

module.exports = schema
