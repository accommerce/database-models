const mongoose = require('mongoose')
const Schema = mongoose.Schema

const shopSchema = new Schema(
    {
        seller: {
            type: mongoose.Types.ObjectId,
            ref: 'Users',
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        address: {
            type: String,
            required: true,
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        approvalStatus: {
            type: String,
            enum: ['pending', 'approved', 'rejected'],
            default: 'pending',
        },
        deletedAt: {
            type: Date,
        },
    },
    { timestamps: true }
)

module.exports = shopSchema
