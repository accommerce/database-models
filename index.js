const mongoose = require('mongoose')
const {
    CartItem,
    Comment,
    Discount,
    Order,
    OrderItem,
    Product,
    User,
    Shop,
    Size,
    ProductStatistics,
    OrderStatistics,
} = require('./schemas')

module.exports = (
    mongoUri,
    options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        maxPoolSize: 10,
    },
    debug = true
) => {
    const connect = mongoose.createConnection(mongoUri, options)
    mongoose.set('debug', debug)

    connect.on('connected', () => console.log('Connected to mongodb: ', mongoUri))
    connect.on('error', (err) => console.error('Error connecting to mongodb: ', err))
    connect.on('close', () => console.log('Closed mongodb: ', mongoUri))

    return {
        CartItems: connect.model('CartItems', CartItem),
        Comments: connect.model('Comments', Comment),
        Discounts: connect.model('Discounts', Discount),
        Orders: connect.model('Orders', Order),
        OrderItems: connect.model('OrderItems', OrderItem),
        Products: connect.model('Products', Product),
        Sizes: connect.model('Sizes', Size),
        Users: connect.model('Users', User),
        Shops: connect.model('Shops', Shop),
        ProductStatistics: connect.model('ProductStatistics', ProductStatistics),
        OrderStatistics: connect.model('OrderStatistics', OrderStatistics),
        Connection: connect,
    }
}
